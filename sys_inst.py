from src import sys_inst


if __name__ == '__main__':
    sys_inst.pre.main()
    inst_params = sys_inst.params.prompt()
    sys_inst.inst.main(inst_params)
    # postinstall.goto_arch_setup(install_params)