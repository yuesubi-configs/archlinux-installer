from .utils import *



PACMAN_ARGS = "--noconfirm --color=auto"

DOTFILES_REPO = "https://gitlab.com/yuesubi-configs/dotfiles.git"

SOFWARE_PACKAGES = [
    "adobe-source-han-sans-jp-fonts",   # Japanese font
    "adobe-source-han-serif-jp-fonts",  # Japanese font
    "alacritty",                        # Terminal emulator
    "breeze",                           # Breeze Qt theme
    "breeze-gtk",                       # Breeze Gtk theme
    "cifs-utils",                       # Mount windows shares
    "code",                             # Text editor
    "dmenu",                            # App launcher
    "dosfstools",                       # ?
    "dunst",                            # Notification server
    "exa",                              # Better ls
    "firefox",                          # Web browser
    "fish",                             # Better shell
    "git",                              # Git
    "htop",                             # TUI system monitor
    "libreoffice-still",                # Libre office
    "lxappearance",                     # Tool to set the gtk theme
    "neofetch",                         # Eye candy logo
    "neovim",                           # Better vim
    "network-manager-applet",           # Network manager sys tray applet 
    "noto-fonts-emoji",                 # Emoji font
    "ntfs-3g",                          # Mount NTFS file systems
    "numlockx",                         # Turn on numlock for X
    "openssh",                          # SSH client
    "pcmanfm",                          # GUI file explorer
    #"pipewire",                         # Sound server
    "python",                           # Python 3
    "python-pygame",                    # Pygame library for python
    "qt6ct",                            # Tool to set the Qt theme
    "starship",                         # Better prompt
    "ttf-dejavu",                       # Common font aliased as sherif & sans-sherif
    "ttf-fira-code",                    # Programming font
    "ttf-hanazono",                     # Japanese font
    "ttf-liberation",                   # Libre office default font
    "ttf-sazanami",                     # Japanese font
    "vim",                              # TUI text editor
    "vlc",                              # Video player
]

GPU_DRIVERS = [
    "xf86-video-vesa",
    "xf86-video-ati",
    "xf86-video-intel",
    "xf86-video-amdgpu",
    "xf86-video-nouveau",
    "xf86-video-fbdev"
]



def _boot_setup(params: SetupParams) -> None:
    # Micro code
    run_command(f"pacman -S amd-ucode intel-ucode {PACMAN_ARGS}")
    
    # Num lock on startup
    install_aur_package("mkinitcpio-numlock", params.username)

    lines = list()
    with open("/etc/mkinitcpio.conf", "r") as mkinitcpio_conf:
        lines = mkinitcpio_conf.readlines()
    for l, line in enumerate(lines):
        if ("HOOKS=" in line) and ("#" not in line):
            index = line.find(")")
            new_line = line[:index] + " numlock" + line[index:]
            if "encrypt" in line:
                index = line.find("encrypt")
                new_line = line[:index] + "numlock " + line[index:]
            lines[l] = new_line
            break
    with open("/etc/mkinitcpio.conf", "w") as mkinitcpio_conf:
        mkinitcpio_conf.writelines(lines)
    
    run_command(f"mkinitcpio -P")


def _install_xorg(params: SetupParams) -> None:
    if params.is_removable:
        run_command(f"pacman -S {' '.join(GPU_DRIVERS)} {PACMAN_ARGS}")

    run_all_commands([
        f"pacman -S xf86-video-fbdev xf86-video-amdgpu xf86-video-intel " +
            f"xf86-video-nouveau {PACMAN_ARGS}",
        f"pacman -S xorg xorg-xinit {PACMAN_ARGS}",
        f"pacman -S picom vim firefox alacritty nitrogen {PACMAN_ARGS}",
    ])


def _install_hyprland(params: SetupParams) -> None:
    # Dependencies
    DEPS = [
        "libxcb", "xcb-proto", "xcb-util", "xcb-util-keysyms", "libxfixes",
        "libx11", "libxcomposite", "xorg-xinput", "libxrender", "pixman",
        "wayland-protocols", "cairo", "pango", "polkit", "glslang", "libinput",
        "libxcb", "libxkbcommon", "opengl-driver", "wayland", "xcb-util-errors",
        "xcb-util-renderutil", "xcb-util-wm", "seatd", "vulkan-icd-loader",
        "vulkan-validation-layers", "xorg-xwayland", "qt5-wayland",
        "qt6-wayland"
    ]
    run_command(f"pacman -S {' '.join(DEPS)} {PACMAN_ARGS}")

    install_aur_package("hyprland-bin", params.username)


def _install_qtile(params: SetupParams) -> None:
    run_command(f"pacman -S qtile python python-psutil {PACMAN_ARGS}")


def perform_setup(params: SetupParams) -> None:



    """
    _config_mkinitcpio(params)
    _shell_setup(params)

    _package_management_setup(params)
    _boot_setup(params)

    _dotfiles_setup(params)

    _install_xorg(params)

    _install_rust(params)
    _install_software(params)

    _install_qtile(params)
    _install_hyprland(params)
    """

    # GUI
    # Xorg and/or wayland
    # amd & nvidia drivers
    # install a window manager
    # display manager (login manager)
    # create user directories

    # MULTIMEDIA
    # ...

    # NETWORKING
    # ...

    # install sofware
