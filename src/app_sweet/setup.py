from ..utils import cmd


DOTFILES_REPO = "https://gitlab.com/yuesubi-configs/dotfiles.git"

PACMAN_ARGS: str = "--noconfirm"


def _git():
    cmd.run(f"sudo pacman -S git {PACMAN_ARGS}")


def _base_devel():
    cmd.run(f"sudo pacman -S base-devel {PACMAN_ARGS}")


def _aur_helper():
    cmd.run_all([
        f"git clone https://aur.archlinux.org/paru.git",
        f'bash -c "cd paru && makepkg -si {PACMAN_ARGS}"',
        f"rm -rf paru",
    ])


def _rust_lang():
    RUST_INST = "rust_inst.sh"
    cmd.run_all([
        f"curl --proto '=https' -o {RUST_INST} -sSf https://sh.rustup.rs",
        f"sh {RUST_INST} -y",
        f'source ".cargo/env"',
        f"rm {RUST_INST}",
    ])


def _python_lang():
    cmd.run(f"sudo pacman -S python python-pip {PACMAN_ARGS}")


def _shell():
    cmd.run_all([
        f"sudo pacman -S fish starship exa neovim links htop neofetch {PACMAN_ARGS}",
        f"chsh -s /usr/bin/fish",
        f"set -U fish_greeting",
    ])


def _win_fs():
    cmd.run(f"sudo pacman -S cifs-utils ntfs-3g {PACMAN_ARGS}")

def _ssh_client():
    cmd.run(f"sudo pacman -S openssh {PACMAN_ARGS}")

def _dotfiles():
    cmd.run_all([
        f"git clone {DOTFILES_REPO}",
        f"python dotfiles/dotfiles.py -c arch",
    ])


def main():
    _git()
    _base_devel()
    _aur_helper()

    _rust_lang()
    _python_lang()

    _shell()
    _win_fs()
    _ssh_client()

    _dotfiles()