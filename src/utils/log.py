import os
import logging


LOG_DIRECTORY = os.path.join(os.path.dirname(__file__), "..", "..", "log")

FILE_FORMATTER = logging.Formatter(
    "%(asctime)s %(levelname)s : %(message)s", 
    datefmt="%Y-%m-%d %H:%M:%S"
)
STREAM_FORMATTER = logging.Formatter(
    "\033[37m%(asctime)s\033[0m \033[95m%(levelname)s\033[0m " +
    "\033[37m:\033[0m %(message)s", 
    datefmt="%H:%M:%S"
)


class Log(logging.Logger):
    def __init__(self, file_name: str) -> None:
        super().__init__(self, logging.INFO)

        if not os.path.exists(LOG_DIRECTORY):
            os.makedirs(LOG_DIRECTORY)

        log_file = os.path.join(LOG_DIRECTORY, file_name)
        file_handler = logging.FileHandler(log_file, "a", encoding="utf-8")
        file_handler.setFormatter(FILE_FORMATTER)
        file_handler.setLevel(logging.INFO)

        stream_handler = logging.StreamHandler()
        stream_handler.setFormatter(STREAM_FORMATTER)
        stream_handler.setLevel(logging.INFO)

        self.addHandler(file_handler)
        self.addHandler(stream_handler)


log = Log("res.log")
__all__ = ["log"]