import subprocess

from .log import log


def run_all(commands: list[str]) -> bool:
    success = True
    for command in commands:
        success = success and run(command)
    return success


def run(command: str) -> bool:
    # Log message
    log.info(f"\033[34mrun\033[0m : {command}")

    # Run the command
    process = subprocess.Popen(
        [command],
        shell=True,
        stdout=subprocess.PIPE, stderr=subprocess.PIPE
    )

    # Live stdout output
    line = b""
    for c in iter(lambda: process.stdout.read(1), b""):
        if c == b'\n':
            log.info(f"\033[92mout\033[0m : {line.decode()}")
            line = b""
        else:
            line += c
    if len(line) > 0:
        log.info(f"\033[92mout\033[0m : {line.decode()}")

    # Print the stderr output
    for line in process.stderr.readlines():
        to_log = line.decode().rstrip("\n")
        log.info(f"\033[33merr\033[0m : {to_log}")
    
    # Result
    result = process.wait()
    log.info(f"\033[96mexit\033[0m : {result}")
    if result != 0:
        log.warning(f"\033[91mfail\033[0m : {command}")
        return False
    return True


def eval(command: str) -> str:
    # Log message
    log.info(f"\033[34meval\033[0m : {command}")

    # Run the command
    process = subprocess.Popen(
        [command],
        shell=True,
        stdout=subprocess.PIPE, stderr=subprocess.PIPE
    )

    # Live stderr output
    line = b""
    for c in iter(lambda: process.stderr.read(1), b""):
        if c == b'\n':
            log.info(f"\033[33merr\033[0m : {line.decode()}")
            line = b""
        else:
            line += c
    if len(line) > 0:
        log.info(f"\033[33merr\033[0m : {line.decode()}")

    output = "".join([
        line.decode()
        for line in process.stdout.readlines()
    ])
    
    # Result
    result = process.wait()
    log.info(f"\033[96mexit\033[0m : {result}")
    if result != 0:
        log.warning(f"\033[91mfail\033[0m : {command}")
    
    return output


__all__ = ["run", "run_all", "eval"]