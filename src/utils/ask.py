from typing import TypeVar, Callable


T = TypeVar("T")


def between(choices: list[T], message: str) -> T:
    while True:
        print(message)
        for c, choice in enumerate(choices):
            print(f"{c + 1} - {choice}")
        try:
            choice_done = int(input("\033[105;30m  \033[0m> ")) - 1
            if 0 <= choice_done < len(choices):
                return choices[choice_done]
        except ValueError:
            pass
        print("\033[31mInvalid answer!\033[0m")
    

def yes_no(question: str) -> bool:
    while True:
        responce = input(
            f"{question} [\033[32my\033[0m/\033[31mn\033[0m] "
        ).strip(" \n\r").lower()
        if responce == "y":
            return True
        elif responce == "n":
            return False
        else:
            print("\033[31mInvalid answer!\033[0m")


def prompt(prompt_txt: str, validator: Callable[[str], bool]) -> str:
    while True:
        given_input = input(prompt_txt)
        if validator(given_input):
            return given_input
        print("\033[31mInvalid input !\033[0m")


__all__ = ["between", "yes_no", "prompt"]