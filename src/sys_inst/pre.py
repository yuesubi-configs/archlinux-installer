from ..utils import cmd


def main():
    cmd.run_all([
        "ping -c 1 google.com",
        "timedatectl status"
    ])