from ..utils import cmd
from . import params


MNT_PATH: str = "/mnt"
ROOT_MP: str = MNT_PATH
HOME_MP: str = ROOT_MP + "/home"
EFI_PATH: str = "/boot/efi"
EFI_MP: str = ROOT_MP + EFI_PATH

LANG: str = "en_US.UTF-8"
KEYMAP: str = "fr-latin1"

LOCS: list[str] = [
    "en_US.UTF-8 UTF-8",
    "fr_FR.UTF-8 UTF-8",
    "ja_JP.UTF-8 UTF-8",
]

CHROOT_PRE: str = f"arch-chroot {ROOT_MP} "
PACMAN_ARGS: str = "--noconfirm --color=auto"


def _format_parts(p: params.Params):
    cmd.run(f"mkfs.btrfs -f -L Arch {p.root_part}")
    if p.use_home_part and p.format_home:
        cmd.run(f"mkfs.btrfs -f -L Home {p.home_part}")
    if p.use_swap_part:
        cmd.run(f"mkswap {p.swap_part}")
    if p.format_efi:
        cmd.run(f"mkfs.fat -F 32 {p.efi_part}")


def _mount_parts(p: params.Params):
    cmd.run(f"mount {p.root_part} {ROOT_MP}")
    if p.use_home_part:
        cmd.run(f"mount --mkdir {p.home_part} {HOME_MP}")
    cmd.run(f"mount --mkdir {p.efi_part} {EFI_MP}")

    if p.use_swap_part:
        cmd.run(f"swapon {p.swap_part}")


def _pacstrap(p: params.Params):
    cmd.run(f"pacstrap -K {ROOT_MP} base linux linux-firmware")


def _genfstab(p: params.Params):
    cmd.run(f"genfstab -U /mnt >> /mnt/etc/fstab")


def _time_zone(p: params.Params):
    cmd.run(CHROOT_PRE + f"ln -sf /usr/share/zoneinfo/Europe/Paris /etc/localtime")
    cmd.run(CHROOT_PRE + f"hwclock --systohc")


def _locales(p: params.Params):
    with open(ROOT_MP + "/etc/locale.gen", "a") as loc_gen:
        loc_gen.writelines([
            f"\n{line}\n"
            for line in LOCS
        ])
    cmd.run(CHROOT_PRE + f"locale-gen")

    with open(ROOT_MP + "/etc/locale.conf", "a") as locale_conf:
        locale_conf.writelines([f"\nLANG={LANG}\n"])
    with open(ROOT_MP + "/etc/vconsole.conf", "a") as vconsole_conf:
        vconsole_conf.writelines([f"\nKEYMAP={KEYMAP}\n"])

    
def _hostname(p: params.Params):
    with open(ROOT_MP + "/etc/hostname", "w") as hostname:
        hostname.writelines([p.hostname])


def _net_man(p: params.Params):
    cmd.run_all([
        CHROOT_PRE + f"pacman -S networkmanager {PACMAN_ARGS}",
        CHROOT_PRE + f"systemctl enable NetworkManager.service"
    ])


def _initramfs(p: params.Params) -> None:
    if p.is_removable:
        lines = list()
        with open(HOME_MP + "/etc/mkinitcpio.conf", "r") as mkinitcpio_conf:
            lines = mkinitcpio_conf.readlines()
        for l, line in enumerate(lines):
            if ("HOOKS=" in line) and ("#" not in line):
                open_paren = line.find("(") + 1
                closed_paren = line.find(")")
                hooks = line[open_paren:closed_paren].split(" ")
                hooks.remove("keyboard")
                hooks.remove("block")
                pos = hooks.index("autodetect")
                hooks.insert(pos, "block")
                hooks.insert(pos, "keyboard")
                lines[l] = "HOOKS=(" + " ".join(hooks) + ")\n"
        with open(HOME_MP + "/etc/mkinitcpio.conf", "w") as mkinitcpio_conf:
            mkinitcpio_conf.writelines(lines)
    
        cmd.run(CHROOT_PRE + f"mkinitcpio -P")


def _user(p: params.Params):
    cmd.run_all([
        CHROOT_PRE + f'bash -c "echo \'root:{p.password}\' | chpasswd"',
        CHROOT_PRE + f"useradd -m {p.username}",
        CHROOT_PRE + f'bash -c "echo \'{p.username}:{p.password}\' | chpasswd"'
    ])


def _user_dirs(p: params.Params):
    cmd.run_all([
        CHROOT_PRE + f"pacman -S xdg-user-dirs {PACMAN_ARGS}",
        CHROOT_PRE + f"xdg-user-dirs-update"
    ])


def _sudo(p: params.Params):
    cmd.run_all([
        CHROOT_PRE + f"pacman -S sudo {PACMAN_ARGS}",
        CHROOT_PRE + f"usermod -aG wheel {p.username}",
        f"cp {ROOT_MP}/etc/sudoers sudoers",
    ])
    with open("sudoers", "a") as sudoers:
        sudoers.writelines([f"\n%wheel\tALL=(ALL:ALL) ALL\n"])
    cmd.run_all([
        f"visudo -c sudoers",
        f"rm {ROOT_MP}/etc/sudoers",
        f"cp sudoers {ROOT_MP}/etc/sudoers",
        f"rm sudoers",
    ])


def _microcode(p: params.Params):
    cmd.run(CHROOT_PRE + f"pacman -S amd-ucode intel-ucode {PACMAN_ARGS}")


def _grub(p: params.Params):
    cmd.run_all([
        CHROOT_PRE + f"pacman -S grub efibootmgr {PACMAN_ARGS}",
        CHROOT_PRE + f"grub-install --target=x86_64-efi "
            f"--efi-directory={EFI_PATH} --bootloader-id=arch --recheck" +
            (" --removable" if p.is_removable else ""),
        CHROOT_PRE + f"grub-mkconfig -o /boot/grub/grub.cfg",
    ])


def _pacman(p: params.Params):
    cmd.run(CHROOT_PRE + f"pacman -S reflector {PACMAN_ARGS}")

    pacman_conf_lines = list()
    with open(f"{ROOT_MP}/etc/pacman.conf", "r") as pacman_conf:
        pacman_conf_lines = pacman_conf.readlines()
    for l, line in enumerate(pacman_conf_lines):
        if "[options]" in line:
            pacman_conf_lines.insert(l + 1, "\nColor\n")
            pacman_conf_lines.insert(l + 2, "ParallelDownloads = 5\n")
            break
    with open(f"{ROOT_MP}/etc/pacman.conf", "w") as pacman_conf:
        pacman_conf.writelines(pacman_conf_lines)


def main(p: params.Params):
    _format_parts(p)
    _mount_parts(p)
    _pacstrap(p)
    _genfstab(p)

    _time_zone(p)
    _locales(p)
    _hostname(p)
    _net_man(p)
    _initramfs(p)
    _user(p)
    _user_dirs(p)
    _sudo(p)
    _microcode(p)
    _grub(p)

    _pacman(p)


    # display server
    # display drivers
    # compositor
    # (display manager)
    # cbatticon
    # pulse audio


    # firewall
    # cifs
    # fira