import json

from ..utils import ask, cmd


class Params:
    def __init__(self) -> None:
        # Partition parameters
        self.target_dev: str = str()

        self.use_swap_part: bool = False
        self.use_home_part: bool = False

        self.format_efi: bool = False
        self.format_home: bool = False

        self.efi_part: str = str()
        self.swap_part: str = str()
        self.root_part: str = str()
        self.home_part: str = str()

        # Setup parameters
        self.hostname: str = str()
        self.username: str = str()
        self.password: str = str()

        self.is_removable: bool = False
    
    def print_part(self) -> None:
        print(f"Partition parameters:")

        print(f"target:")
        print(f"- {self.target_dev}")

        print(f"partitions:")
        print(f"- efi {self.efi_part} format={self.format_efi}")
        if self.use_swap_part:
            print(f"- swap {self.swap_part} format=True")
        print(f"- root {self.root_part} format=True")
        if self.use_home_part:
            print(f"- home {self.home_part} format={self.format_efi}")

    def print_setup(self) -> None:
        print(f"Setup parameters:")

        print(f"- hostname: {self.hostname}")
        print(f"- username: {self.username}")
        print(f"- password: {self.password}")
        print(f"- {'is' if self.is_removable else 'is not'} removable")


def _prompt_part(p: Params) -> None:
    # Target device
    lsblk_info = json.loads(cmd.eval("lsblk -J -o NAME"))
    devices = [device["name"] for device in lsblk_info["blockdevices"]]

    cmd.run(f"lsblk -o NAME,FSTYPE,SIZE,MOUNTPOINTS")
    device_name = ask.between(devices, "Chose the device to install to:")
    p.target_dev = "/dev/" + device_name
    
    # Partition options
    p.use_swap_part = ask.yes_no(
        "Do you want to use a swap partition ?")
    p.use_home_part = ask.yes_no(
        "Do you want to use a home partition ?")

    # Select partitions
    lsblk_info = json.loads(
        cmd.eval(f"lsblk -J -o NAME {p.target_dev}")
    )
    partition_names = [
        child["name"]
        for child in lsblk_info["blockdevices"][0]["children"]
    ]

    cmd.run(f"lsblk -o NAME,FSTYPE,SIZE,MOUNTPOINTS")
    p.efi_part = ("/dev/" +
        ask.between(partition_names, "Chose the EFI partition"))
    
    cmd.run(f"lsblk -o NAME,FSTYPE,SIZE,MOUNTPOINTS")
    p.root_part = ("/dev/" +
        ask.between(partition_names, "Chose the root partition"))

    if p.use_swap_part:
        cmd.run(f"lsblk -o NAME,FSTYPE,SIZE,MOUNTPOINTS")
        p.swap_part = ("/dev/" +
            ask.between(partition_names, "Chose the swap partition"))

    if p.use_home_part:
        cmd.run(f"lsblk -o NAME,FSTYPE,SIZE,MOUNTPOINTS")
        p.home_part = ("/dev/" +
            ask.between(partition_names, "Chose the home partition"))

    # Format options
    p.format_efi = ask.yes_no("Do you want to format the efi partition ?")
    if p.use_home_part:
        p.format_home = ask.yes_no(
            "Do you want to format the home partition ?")


def _prompt_setup(p: Params) -> None:
    def validator(s: str) -> bool:
        for c in s:
            if not ((c.isalnum() and c.isascii()) or c in "-_"):
                return False
        return True
    
    p.hostname = ask.prompt("Gimme hostname: ", validator)
    p.username = ask.prompt("Gimme username: ", validator)

    password_valid = False
    while not password_valid:
        p.password = input("Gimme password: ")
        confirmation = input("Confirm your password: ")
        password_valid = p.password == confirmation
        if not password_valid:
            print("Passwords do not match !")
    
    p.is_removable = ask.yes_no(
        "Should the installation be removable ? [y/n] ",
    )

    return p


def prompt() -> Params:
    p = Params()

    while True:
        _prompt_part(p)
        p.print_part()
        if ask.yes_no("Are those partition parameters correct ?"):
            break

    while True:
        _prompt_setup(p)
        p.print_setup()
        if ask.yes_no("Are those setup parameters correct ?"):
            break
    
    return p