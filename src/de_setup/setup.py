from ..utils import cmd


PACMAN_ARGS: str = "--noconfirm"


def _hyprland():
    cmd.run(f"paru -S hyprland-git {PACMAN_ARGS}")
    cmd.run(f"sudo pacman -S dunst polkit-kde-agent " +
        f"xdg-desktop-portal-hyprland qt5-wayland qt6-wayland " +
        f"cliphist brightnessctl nemo hyprpaper {PACMAN_ARGS}")
    # socat jq # workspaces scirpt




def _browser():
    cmd.run(f"sudo pacman -S firefox {PACMAN_ARGS}")


def _term():
    cmd.run(f"sudo pacman -S kitty {PACMAN_ARGS}")


def _fonts():
    FONTS = [
        "ttf-firacode-nerd",
        "noto-fonts", "noto-fonts-cjk", "noto-fonts-emoji",
        "ttf-nerd-fonts-symbols"
    ]
    cmd.run(f"sudo pacman -S " + ' '.join(FONTS) + f" {PACMAN_ARGS}")


def main():
    _hyprland()

    _browser()
    _term()

    _fonts()


    # hicolor-icon-theme beautyline  # icon font
    # gsettings set org.desktop.interface gtk-theme Sweet
    # gsettings set org.desktop.interface icon-theme BeautyLine
    # code
    # imv  # image view
    # thunderbird
    # libreoffice-fresh
    # vlc gimp gparted

    # mail notif
    # bitwarden cli
    # better image viewer
    # markdown exporter & editor
    # TeX editor
    # notes ?
    # firewall
    # periodic element ?
    # discord
    # ascii art ?